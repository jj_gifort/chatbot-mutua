package tests;
import logger.Log;

import java.util.Properties;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import IntegrationTests.coreLogic.base.*;
import IntegrationTests.coreLogic.android.*;
import IntegrationTests.coreLogic.iOS.*;
import MobileFramework.Base.CreateSession;
import MobileFramework.Properties.PropertiesManager;


/**
 * automated test to verify login to android/iOS app.
 */
public class LoginCorrectoTest extends CreateSession {
	
	LoginCorrectoCoreLogic loginCoreLogic;
	Properties localeConfigProp;
	String userName;
	String password;
	/** 
	 * this method instantiate required helpers depending on the platform(android or iOS)
	 * @param invokeDriver android or iOS
	 */
	@Parameters({"os"})
	@BeforeMethod
	public void instantiateHelpers(String invokeDriver){
		
		PropertiesManager pM = new PropertiesManager();
		localeConfigProp = pM.getLocaleConfigProp();
		
		userName = localeConfigProp.getProperty("userName");
		password = localeConfigProp.getProperty("password");
		
		if (invokeDriver.equalsIgnoreCase("android")){
			loginCoreLogic = new AndroidLoginCorrecto(driver);
		}																		         
		else if (invokeDriver.equalsIgnoreCase("iOS")){
			loginCoreLogic = new IOSLoginCorrecto(driver);
		}
	}

	/** 
	 * method to verify login to android and iOS app
	 * @throws InterruptedException Thrown when a thread is waiting, sleeping, 
	 * or otherwise occupied, and the thread is interrupted, either before
	 *  or during the activity.
	 */ 
	@Test (priority = 1, description="TEST - DAR PARTE CHATBOT")
	
	public void accesoLogin() throws InterruptedException {
		Log.info("START DAR PARTE CHATBOT TEST");
		
		Log.info("Running Public Menu Access");
		loginCoreLogic.accesoMenuPublico();
		Log.info("Verified public menu access");
		
		Log.info("Running Login access");
		loginCoreLogic.accesoLogin();
		Log.info("Verified login page");
		
		Log.info("Running enter user");
		loginCoreLogic.sendEmail(userName);
		Log.info("Verified enter user");
		
		Log.info("Running enter password");
		loginCoreLogic.sendPass(password);
		Log.info("Verified enter password");
		
		Log.info("Running tap buttom");
		loginCoreLogic.iniciarSesion();
		Log.info("Verified tap buttom");
		
//		Log.info("Running Verificar inicio sesi�n");
//		loginCoreLogic.verificarInicioSesion();
//		Log.info("Verified Verificar inicio sesi�n");
		
		Log.info("Running Private Menu Access");
		loginCoreLogic.accesoMenuPrivado();
		Log.info("Verified private menu access");
		
		Log.info("Open Chatbot");
		loginCoreLogic.abrirEme();
		Log.info("Verified open chatbot");
		
		Log.info("Running tap button");
		loginCoreLogic.darParte();
		Log.info("Verified tap Dar Parte");
		
		Log.info("Running tapp button");
		loginCoreLogic.pulsarAuto();
		Log.info("Verified tap AUTO");
		
		Log.info("Running tapp link");
		loginCoreLogic.darParteAhora();
		Log.info("Verified tap link Dar Parte Ahora");
		
		Log.info("Running tapp button");
		loginCoreLogic.pulsarAtras();
		Log.info("Verified tap button");
		
		
		Log.info("END DAR PARTE TEST");
		
	}
}
