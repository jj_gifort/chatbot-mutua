package IntegrationTests.screens.iOS;

import org.openqa.selenium.By;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.FindsByAccessibilityId;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import static java.lang.System.out;

import java.util.List;


/**
 * contains all the locators present on the Cuadro M�dico screen of iOS app.
 */
public class IOSCuadroMedicoScreen extends GenericMethods {
	AppiumDriver driver= null;
	
	
	public IOSCuadroMedicoScreen(AppiumDriver driver) {
		super(driver);
		this.driver =  driver;
		String id;
		// TODO Auto-generated constructor stub
	}
	

	// Locators Login Screen 
	//public By entrarAreaPrivada = By.xpath("");
	public By btnCercaDeMi = By.id("Cerca de mi");
	public By especialidad = By.id("Especialidad, prueba, tratamiento...");
	public By btnBuscar = By.id("Buscar");
}
	
	


