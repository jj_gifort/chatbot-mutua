package IntegrationTests.screens.iOS;

import org.openqa.selenium.By;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.FindsByAccessibilityId;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import static java.lang.System.out;

import java.util.List;


/**
 * contains all the locators present on the login screen of iOS app.
 */
public class IOSLoginScreen extends GenericMethods {
	AppiumDriver driver= null;
	
	
	public IOSLoginScreen(AppiumDriver driver) {
		super(driver);
		this.driver =  driver;
		String id;
		// TODO Auto-generated constructor stub
	}
	

	// Locators Login Screen 
	//public By entrarAreaPrivada = By.xpath("");
	public By entrarAreaPrivada = By.id("Entrar �rea privada");
	public By email = By.xpath("//XCUIElementTypeApplication[@name=\"Asisa\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField");
	public By password = By.xpath("//XCUIElementTypeApplication[@name=\"Asisa\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeScrollView/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeSecureTextField");
	public By btnEntrar = By.xpath("//XCUIElementTypeButton[@label=\"Entrar\"]");
	public By btnNoSoy = By.xpath("//XCUIElementTypeButton[contains(@label,'No soy')]");
	public By labelBienvenido = By.xpath(("//XCUIElementTypeApplication[@name=\"Asisa\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeScrollView/XCUIElementTypeOther[1]"));
	
}
	
	


