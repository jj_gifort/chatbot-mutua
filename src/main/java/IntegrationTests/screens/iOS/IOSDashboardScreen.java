package IntegrationTests.screens.iOS;

import org.openqa.selenium.By;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.FindsByAccessibilityId;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import static java.lang.System.out;

import java.util.List;


/**
 * contains all the locators present on the dashborad screen of iOS app.
 */
public class IOSDashboardScreen extends GenericMethods {
	AppiumDriver driver= null;
	
	
	public IOSDashboardScreen(AppiumDriver driver) {
		super(driver);
		this.driver =  driver;
		String id;
		// TODO Auto-generated constructor stub
	}
	

	// Locators Login Screen 
	//public By entrarAreaPrivada = By.xpath("");
	public By labelTusMedicos = By.id("Tus M�dicos");
	public By buscarMedico = By.id("Buscar m�dico");
	public By menuLateral = By.id("MenuNormal");
	//public By perfilYAyuda = By.name("Perfil y ajustes �");
	public By perfilYAyuda = By.xpath("//XCUIElementTypeApplication[@name=\"Asisa\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]");
}
	
	


