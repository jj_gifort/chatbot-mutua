package IntegrationTests.screens.android;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;



/**
 * contains all the locators present on the login Screen
 */
public class AndroidMenuScreen extends GenericMethods {

	public AndroidMenuScreen(AppiumDriver driver) {
		super(driver);
		
		// TODO Auto-generated constructor stub
	}

	// Locators Menu Screen

	public By iniciarSesion = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.TextView");
	public By chatbot= By.id("com.mutua.mutua:id/cbFloatingButton");
	
}

