package IntegrationTests.screens.android;

import org.openqa.selenium.By;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;



/**
 * contains all the locators present on the login Screen
 */
public class AndroidEmeScreen extends GenericMethods {

		public AndroidEmeScreen(AppiumDriver driver) {
			super(driver);
		
		// TODO Auto-generated constructor stub
		}

	// Eme elements
		public By darParte = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
		public By auto = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.TextView");
		public By darParteAhora = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[5]/android.widget.TextView");
		public By back = By.id("com.mutua.mutua:id/rl_back");
		public By emeField = By.id("com.mutua.mutua:id/etText");
		public By codigoCinesa= By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]/android.widget.TextView");
		public By verCodigo = By.id("com.mutua.mutua:id/btn_type_action");
		public By cerrar = By.id("com.mutua.mutua:id/iv_close");
}

