package IntegrationTests.screens.android;

import org.openqa.selenium.By;

import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;



/**
 * contains all the locators present on the login Screen
 */
public class AndroidDashboardScreen extends GenericMethods {

	public AndroidDashboardScreen(AppiumDriver driver) {
		super(driver);
		
		// TODO Auto-generated constructor stub
	}

	// Dashboard elements
	public By menu = By.id("com.mutua.mutua:id/iv_menu_navigation_drawer");
	public By namePrivate = By.id("com.mutua.mutua:id/tv_username");
	
}

