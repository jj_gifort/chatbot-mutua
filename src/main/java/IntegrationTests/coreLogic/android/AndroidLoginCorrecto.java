package IntegrationTests.coreLogic.android;

import logger.Log;
import IntegrationTests.screens.android.AndroidDashboardScreen;
import IntegrationTests.screens.android.AndroidEmeScreen;
import IntegrationTests.screens.android.AndroidLoginScreen;
import IntegrationTests.screens.android.AndroidMenuScreen;
import MobileFramework.Base.GenericMethods;
import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import IntegrationTests.coreLogic.base.*;


/**
 * contains all methods to login on android app
 */
public class AndroidLoginCorrecto extends LoginCorrectoCoreLogic {
    AndroidLoginScreen androidLoginScreen;
    AndroidDashboardScreen androidDashboardScreen;
    AndroidMenuScreen	androidMenuScreen;
    AndroidEmeScreen androidEmeScreen;

    AppiumDriver driver= null;

    
    public AndroidLoginCorrecto(AppiumDriver driver) {
        androidLoginScreen = new AndroidLoginScreen(driver);
        androidDashboardScreen = new AndroidDashboardScreen(driver);
        androidMenuScreen = new AndroidMenuScreen(driver);
        androidEmeScreen = new AndroidEmeScreen(driver);
        Log.info("ANDROID");
        this.driver =  driver;
    }

    /**
     * method to login to android app
     *
     * @param userName emailId to be used for login
     * @param password - valid password
     */
   // @Override
    @Step ("1.Pulsar bot�n -Men� �rea p�blica-")
    public void accesoMenuPublico() throws InterruptedException {
        
    	androidDashboardScreen.waitForVisibility(androidDashboardScreen.menu);
    	androidDashboardScreen.findElement(androidDashboardScreen.menu).click();

    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("2.Pulsar bot�n -Iniciar Sesi�n")
    public void accesoLogin() throws InterruptedException {
        
    	
    	androidMenuScreen.waitForVisibility(androidMenuScreen.iniciarSesion);
    	androidMenuScreen.findElement(androidMenuScreen.iniciarSesion).click();

    	Thread.sleep(2000);
    	try {
    		GenericMethods.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    		
    
    @Step ("3.Introducir Email")
    public void sendEmail(String userName) throws InterruptedException {
        
    	Thread.sleep(2000);
    	if(androidLoginScreen.isElementPresent(androidLoginScreen.usuarioRecordado)) {
    		androidLoginScreen.findElement(androidLoginScreen.usuarioRecordado).click();
    	}
    	androidLoginScreen.waitForVisibility(androidLoginScreen.userField);
    	androidLoginScreen.findElement(androidLoginScreen.userField).sendKeys(userName);

    	Thread.sleep(2000);
    	try {
    		GenericMethods.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("4. Introducir Contrase�a")
    public void sendPass(String password) throws InterruptedException {
         		

    	androidLoginScreen.findElement(androidLoginScreen.passField).sendKeys(password);
    	
    	Thread.sleep(2000);
    	try {
    		GenericMethods.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("5. Pulsar el bot�n -Iniciar Sesi�n-")
    public void iniciarSesion() throws InterruptedException {
         		

    	androidLoginScreen.waitForVisibility(androidLoginScreen.iniciarSesion);
    	androidLoginScreen.findElement(androidLoginScreen.iniciarSesion).click();
    	
    	Thread.sleep(2000);
    	try {
    		GenericMethods.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("6.Pulsar bot�n -Men� �rea privada-")
    public void accesoMenuPrivado() throws InterruptedException {
        
    	androidDashboardScreen.waitForVisibility(androidDashboardScreen.menu);
    	androidDashboardScreen.findElement(androidDashboardScreen.menu).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("7.Abrir el Chatbot")
    public void abrirEme() throws InterruptedException {
        
    	androidMenuScreen.waitForVisibility(androidMenuScreen.chatbot);
    	androidMenuScreen.findElement(androidMenuScreen.chatbot).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("8.Pulsar bot�n -Dar Parte-")
    public void darParte() throws InterruptedException {
        
    	androidEmeScreen.waitForVisibility(androidEmeScreen.darParte);
    	androidEmeScreen.findElement(androidEmeScreen.darParte).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("9.Pulsar bot�n -Auto-")
    public void pulsarAuto() throws InterruptedException {
        
    	androidEmeScreen.waitForVisibility(androidEmeScreen.auto);
    	androidEmeScreen.findElement(androidEmeScreen.auto).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("10.Pulsar enlace -Dar parte ahora-")
    public void darParteAhora() throws InterruptedException {
        
    	Thread.sleep(2000);
    	androidEmeScreen.waitForVisibility(androidEmeScreen.darParteAhora);
    	androidEmeScreen.findElement(androidEmeScreen.darParteAhora).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    
    @Step ("11.Pulsar bot�n -Back-")
    public void pulsarAtras() throws InterruptedException {
        
    	androidEmeScreen.waitForVisibility(androidEmeScreen.back);
    	androidEmeScreen.findElement(androidEmeScreen.back).click();	
    	 
    	Thread.sleep(2000);
    	 try {
     		GenericMethods.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }

//	@Override
//	public void verificarInicioSesion() throws InterruptedException {
//		androidDashboardScreen.waitForVisibility(androidDashboardScreen.namePrivate);
//		Thread.sleep(2000);
//    	try {
//    		GenericMethods.takeRemoteScreenshot(driver);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	}


}

