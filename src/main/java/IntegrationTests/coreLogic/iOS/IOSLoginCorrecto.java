package IntegrationTests.coreLogic.iOS;

import IntegrationTests.screens.iOS.IOSDashboardScreen;
import IntegrationTests.screens.iOS.IOSLoginScreen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.qameta.allure.Step;
import logger.Log;

import static org.testng.Assert.assertTrue;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import IntegrationTests.coreLogic.base.*;



/**
 * contains method to login on iOS app
 */
public class IOSLoginCorrecto extends LoginCorrectoCoreLogic{
	IOSLoginScreen iosLoginScreen;
	IOSDashboardScreen iOSDashboardScreen;

	AppiumDriver driver= null;

	public IOSLoginCorrecto(AppiumDriver driver){
		iosLoginScreen = new IOSLoginScreen(driver);
		iOSDashboardScreen = new IOSDashboardScreen(driver);
		Log.info("IOS");
		this.driver =  driver;
		
		
	}

/**
 * method to verify login scenario on iOS app
 * @param userName emailId to be used for login
 * @param password - valid password
 */

	@Step ("1.Pulsar bot�n -Entrar �rea privada-")
    public void accesoLogin() throws InterruptedException {
        
		iosLoginScreen.waitForVisibility(iosLoginScreen.entrarAreaPrivada);
	    iosLoginScreen.findElement(iosLoginScreen.entrarAreaPrivada).click();
	    Thread.sleep(2000);
    	try {
    		iosLoginScreen.takeRemoteScreenshot(driver);
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
    	 
    }
    		
    
    @Step ("2. Introducir Email")
    public void sendEmail(String userName) throws InterruptedException {
        
    	if(iosLoginScreen.isElementPresent(iosLoginScreen.btnNoSoy)) {
    		iosLoginScreen.waitForVisibility(iosLoginScreen.btnNoSoy);
    		iosLoginScreen.findElement(iosLoginScreen.btnNoSoy).click();
    	}
    	
    	if(iosLoginScreen.isElementPresent(iosLoginScreen.email)) {
    		iosLoginScreen.findElement(iosLoginScreen.email).click();
    	}
    	iosLoginScreen.waitForVisibility(iosLoginScreen.email);
    	iosLoginScreen.findElement(iosLoginScreen.email).sendKeys(userName);

    	Thread.sleep(2000);
    	try {
    		iosLoginScreen.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("3. Introducir Contrase�a")
    public void sendPass(String password) throws InterruptedException {
         		

    	iosLoginScreen.findElement(iosLoginScreen.password).sendKeys(password);
    	//iosLoginScreen.hideKeyboard();
    	iosLoginScreen.findElement(iosLoginScreen.labelBienvenido).click();
    	
    	Thread.sleep(2000);
    	try {
    		iosLoginScreen.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("4. Pulsar el bot�n -Entrar-")
    public void entrar() throws InterruptedException {
         		
    	//Thread.sleep(2000);
    	iosLoginScreen.waitForVisibility(iosLoginScreen.btnEntrar);
    	iosLoginScreen.findElement(iosLoginScreen.btnEntrar).click();
    	
    	Thread.sleep(2000);
    	try {
    		iosLoginScreen.takeRemoteScreenshot(driver);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Step ("5. Verificar inicio sesi�n")
    public void verificarInicioSesion() throws InterruptedException {
         		
    	//Thread.sleep(2000);
    	if (iOSDashboardScreen.isElementPresent(iOSDashboardScreen.labelTusMedicos)) {
    		assertTrue(true);
    		Thread.sleep(2000);
        	try {
        		iosLoginScreen.takeRemoteScreenshot(driver);
    		} catch (Exception e) {
    			// TODO: handle exception
    		}
    	}
    	else {
    		assertTrue(false);
    		Thread.sleep(2000);
        	try {
        		iosLoginScreen.takeRemoteScreenshot(driver);
    		} catch (Exception e) {
    			// TODO: handle exception
    		}
    	}
    	
    }

	@Override
	public void accesoMenuPublico() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void iniciarSesion() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void accesoMenuPrivado() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void abrirEme() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void darParte() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pulsarAuto() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void darParteAhora() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pulsarAtras() throws InterruptedException {
		// TODO Auto-generated method stub
		
	}

			

}