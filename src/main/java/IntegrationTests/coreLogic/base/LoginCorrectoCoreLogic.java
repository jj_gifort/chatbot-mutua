package IntegrationTests.coreLogic.base;

/**
 * contains all methods which are present in AndroidLoginHelper and IOSLoginHelper.
 * Methods are abstract here and defined in specific class (AndroidLoginHelper and IOSLoginHelper) 
 */
public abstract class LoginCorrectoCoreLogic { 

	public abstract void accesoMenuPublico()
			throws InterruptedException;
	
	public abstract void accesoLogin()
			throws InterruptedException;
	
	public abstract void sendEmail(String userName)
			throws InterruptedException;
	
	public abstract void sendPass(String password)
			throws InterruptedException;

	public abstract void iniciarSesion()
			throws InterruptedException;
	
	public abstract void accesoMenuPrivado()
			throws InterruptedException;
	
	public abstract void abrirEme()
			throws InterruptedException;
	
	public abstract void darParte()
			throws InterruptedException;
	
	public abstract void pulsarAuto()
			throws InterruptedException;
	
	public abstract void darParteAhora()
			throws InterruptedException;
	
	public abstract void pulsarAtras()
			throws InterruptedException;

	 
	 
}
