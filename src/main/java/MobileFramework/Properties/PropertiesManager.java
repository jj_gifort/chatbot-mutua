package MobileFramework.Properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

import logger.Log;

public class PropertiesManager {

	Properties configFile;
	Properties configProp = new Properties();
	protected File file = new File("");
	protected static Properties lobConfigProp = new Properties();
	protected static Properties localeConfigProp = new Properties();
	protected FileInputStream configFis, lobConfigFis, localeConfigFis;
	protected Properties testDataFile;
	private final String CONFIG_FILE_PATH = "//src//main//resources//config//config.properties";
	private static String propFileRoute;
	
	public PropertiesManager() {
		super();
		propFileRoute = CONFIG_FILE_PATH;
	}

	public Properties getLobConfigProp() {
		return lobConfigProp;
	}

	public void setLobConfigProp(Properties lobConfigProp) {
		PropertiesManager.lobConfigProp = lobConfigProp;
	}

	public Properties getLocaleConfigProp() {
		return localeConfigProp;
	}

	public void setLocaleConfigProp(Properties localeConfigProp) {
		PropertiesManager.localeConfigProp = localeConfigProp;
	}

	public String readProperty(String property) {
		Properties properties = new Properties();

		try {
			properties.load(new FileInputStream(System.getProperty("user.dir")+propFileRoute));
		} catch (IOException e) {
			Log.info("There was an error trying to open properties file");
		}
		
		String propValue = properties.getProperty(property);
		Log.info("Property: " + property + " = " + propValue);
		return propValue;
	}

	/**
	 * this method loads properties files config and file having test data.
	 * 
	 * @param platform android or ios, to load specific test data file.
	 * @throws Exception property files are not loaded successfully
	 */
	public void propertiesFileLoad(String platform) throws Exception {
		configFis = new FileInputStream(file.getAbsoluteFile() + CONFIG_FILE_PATH);
		configProp.load(configFis);

		File f = new File(file.getAbsoluteFile() + "//src//main//resources//config//" + platform + "_config.properties");

		if (f.exists() && !f.isDirectory()) {
			lobConfigFis = new FileInputStream(
					file.getAbsoluteFile() + "/src//main//resources//config//" + platform + "_config.properties");
			lobConfigProp.load(lobConfigFis);

			String locale = lobConfigProp.getProperty("LOCALE");

			localeConfigFis = new FileInputStream(file.getAbsoluteFile() + "//src//test//resources//testData//" + locale
					+ "_" + platform + ".properties");
			localeConfigProp.load(localeConfigFis);
			
			
		} else {
			throw new Exception("Properties files loading failed ");
		}
	}

	public void configureLogger() {

		File propertiesFile = new File(file.getAbsoluteFile() + "//src//main//resources//log4j.properties");
		PropertyConfigurator.configure(propertiesFile.toString());	
		
	}
}
