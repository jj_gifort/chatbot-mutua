package MobileFramework.Properties;

public class GlobalProperties {
	
	public static String appiumMacOSHost= null;
	public static String appiumMacOSPort= null;
	public static String appiumHostMC= null;
	public static String appiumPortMC= null;
	//public static String MC_USER= null;
	//public static String MC_PASS= null;
	public static String DEVICE_NAME= null;
	
	
	public static String deviceiOSName= null;
	public static String deviceiOSUDID= null;
	public static String deviceiOSVersion= null;
	public static String iOSAppPath= null;
	public static String applicationBundle= null;

	public static String deviceAndroidName= null;
	public static String deviceAndroidUDID= null;
	public static String deviceAndroidVersion= null;
	public static String AndroidAppPath= null;
	public static String androidPackageName = null;
	public static String androidActivityName = null;

	public static String DBURL= null;
	public static String username= null;
	public static String DBURL_beta= null;
	public static String username_beta= null;
	public static String password_beta= null;
	public static String driver= null;
	public static String screenShotPath= null;

	
	public static void readGlobalProperties(){
		
		PropertiesManager pm = new PropertiesManager();
		
		appiumMacOSHost = pm.readProperty("appiumMacOSHost");
		appiumMacOSPort = pm.readProperty("appiumMacOSPort");
		appiumHostMC = pm.readProperty("appiumHostMC");
		appiumPortMC = pm.readProperty("appiumPortMC");
		//MC_USER = pm.readProperty("MC_USER");
		//MC_PASS = pm.readProperty("MC_PASS");
		DEVICE_NAME = pm.readProperty("DEVICE_NAME");
		deviceiOSName = pm.readProperty("deviceiOSName");
		deviceiOSUDID = pm.readProperty("deviceiOSUDID");
		deviceiOSVersion = pm.readProperty("deviceiOSVersion");
		iOSAppPath = pm.readProperty("iOSAppPath");
		applicationBundle = pm.readProperty("applicationBundle");
		deviceAndroidName = pm.readProperty("deviceAndroidName");
		deviceAndroidUDID = pm.readProperty("deviceAndroidUDID");
		deviceAndroidVersion = pm.readProperty("deviceAndroidVersion");
		androidPackageName = pm.readProperty("androidPackageName");
		androidActivityName= pm.readProperty("androidActivityName");
		AndroidAppPath = pm.readProperty("AndroidAppPath");
		DBURL = pm.readProperty("DBURL");
		DBURL = pm.readProperty("DBURL");
		DBURL_beta = pm.readProperty("DBURL_beta");
		username_beta = pm.readProperty("username_beta");
		password_beta = pm.readProperty("password_beta");
		driver = pm.readProperty("driver");
		screenShotPath = pm.readProperty("screenShotPath");

	}
}