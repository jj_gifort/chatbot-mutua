package MobileFramework.Performance;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import MobileFramework.Properties.GlobalProperties;
import io.appium.java_client.android.AndroidDriver;
import logger.Log;

public class AndroidPerformance implements Runnable {

	private AndroidDriver driver;

	public AndroidPerformance(AndroidDriver driver) {
		this.driver = driver;
	}

	public static List<String> getPerformanceTypesSupported(AndroidDriver driver) throws Exception {
		return driver.getSupportedPerformanceDataTypes();
	}

	public static HashMap<String, Integer> getPerformanceInfo(AndroidDriver driver, String performanceData)
			throws Exception {
		Thread.sleep(2500);
		List<List<Object>> data = driver.getPerformanceData(GlobalProperties.androidPackageName, performanceData, 5);
		HashMap<String, Integer> readableData = new HashMap<>();
		for (int i = 0; i < data.get(0).size(); i++) {
			int val;
			if (data.get(1).get(i) == null) {
				val = 0;
			} else {
				val = Integer.parseInt((String) data.get(1).get(i));
			}
			readableData.put((String) data.get(0).get(i), val);
		}
		return readableData;
	}

	public void showPerformanceMetrics(AndroidDriver driver) throws Exception {

		Log.info("Collecting performance metrics");

		List<String> performanceDataList = AndroidPerformance.getPerformanceTypesSupported(driver);

		for (String performanceData : performanceDataList) {

			if (!performanceData.equals("cpuinfo")) {
				HashMap<String, Integer> info = AndroidPerformance.getPerformanceInfo(driver, performanceData);

				System.out.println("------------" + performanceData + "----------------");

				for (String name : info.keySet()) {
					String key = name.toString();
					String value = info.get(name).toString();
					System.out.println(key + " " + value);
				}
			}
		}
	}

	@Override
	public void run() {
		try {
			showPerformanceMetrics(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
