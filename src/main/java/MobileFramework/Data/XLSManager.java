package MobileFramework.Data;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import jxl.*;
import jxl.read.biff.BiffException;

public class XLSManager {
	
	private Workbook excel;
	private Sheet hojaExcel;
	private String modo;	
	private int filaActual;
	private String mapeo = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private Hashtable<String, String> mapaNombresColumnas;
	
	
	/**
	 * Constructor
	 * @param ficheroXLS
	 * @param hoja
	 * @param modo
	 * @throws IOException 
	 * @throws BiffException 
	 */
	
	public XLSManager(String ficheroExcel, String hoja, String modo){
		try {
			excel = Workbook.getWorkbook(new File(ficheroExcel));
			hojaExcel = excel.getSheet(hoja);
			this.modo = modo;
			
			//Si el modo en que queremos tratar la excel es mediante Keyword Driven Testing (KDT)
			//entonces tenemos que mapear hasta un maximo de 676 columnas
			if (modo.equals("KDT")){
				//Leemos la primera fila, la cual identificar� el nombre de las columnas
			this.mapaNombresColumnas = new Hashtable<>();
			String key = "";
			String value = "";
			for (int j=0; j<hojaExcel.getColumns(); j++){
				if (j<mapeo.length()){
						value = ""+mapeo.charAt(j%mapeo.length()); //Columnas: A, B, C, D, E...
					}else{
						value = ""+mapeo.charAt(j/mapeo.length()-1)+mapeo.charAt(j%mapeo.length()); //Columnas: AA, AB, AC...BA, BB, BC...ZA, ZB, ZC...ZZ
					}
					key = hojaExcel.getCell(j, 0).getContents();
					this.mapaNombresColumnas.put(key, value);
				}
				this.filaActual = 2;
			}			
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public XLSManager(String ficheroExcel, int hoja, String modo){
		
		try {
			excel = Workbook.getWorkbook(new File(ficheroExcel));
			hojaExcel = excel.getSheet(hoja);
			this.modo = modo;
			
			//Si el modo en que queremos tratar la excel es mediante Keyword Driven Testing (KDT)
			//entonces tenemos que mapear hasta un maximo de 676 columnas
			if (modo.equals("KDT")){
				//Leemos la primera fila, la cual identificar� el nombre de las columnas
			this.mapaNombresColumnas = new Hashtable<>();
			String key = "";
			String value = "";
			for (int j=0; j<hojaExcel.getColumns(); j++){
				if (j<mapeo.length()){
						value = ""+mapeo.charAt(j%mapeo.length()); //Columnas: A, B, C, D, E...
					}else{
						value = ""+mapeo.charAt(j/mapeo.length()-1)+mapeo.charAt(j%mapeo.length()); //Columnas: AA, AB, AC...BA, BB, BC...ZA, ZB, ZC...ZZ
					}
					key = hojaExcel.getCell(j, 0).getContents();
					this.mapaNombresColumnas.put(key, value);
				}
				this.filaActual = 2;
			}	
			

		} catch (IOException e) {
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Devuelve el n�mero de filas de la excel actual.
	 * Var�a dependiendo de si el modo en que se trata la excel de datos es mediante KDT, o como una tabla de datos normal.
	 * @return
	 */
	public int getNumFilas(){
		if (modo.equals("KDT")){
			return hojaExcel.getRows()-1;
		}else{
			return hojaExcel.getRows();
		}
	}
	
	
	public int getNumColumnas(){
		return hojaExcel.getColumns();
	}
	
	
	public String getCellData(int i, int j){
		Cell celda = hojaExcel.getCell(j, i);
		return celda.getContents();
	}
	
	
	//******** METODOS PARA KEYWORD DRIVEN TESTING ********/
	
	/**
	 * Obtiene el valor de la celda cuya fila es la que tiene establecido el puntero "filaActual", 
	 * y la columna es la que se pasa como par�metro. (S�lo para KDT)
	 * @param columna
	 * @return
	 * @throws Exception 
	 */
	public String getCellData(int columna) throws Exception{
		if (!this.modo.equals("KDT")){
			throw new Exception("No se puede ejecutar esta acci�n sin activar el modo \"KDT\". Act�velo en el constructor para usar esta funcionalidad");
		}else{
			Cell celda = hojaExcel.getCell(columna, this.filaActual);
			return celda.getContents();
		}
	}
	
	/**
	 * Obtiene el valor de la celda, cuya fila es la que tiene establecido el puntero "filaActual"
	 * y la columna es la que contiene la etiqueta indicada.
	 * Dicha etiqueta ser� la que se ha introducido en la primera fila de la excel. (S�lo para KDT)
	 * @param columna
	 * @return
	 * @throws Exception 
	 */
	public String getCellData(String etiqueta) throws Exception{
		if (!this.modo.equals("KDT")){
			throw new Exception("No se puede ejecutar esta acci�n sin activar el modo \"KDT\". Act�velo en el constructor para usar esta funcionalidad");
		}else{
			Cell celda = hojaExcel.getCell(this.mapaNombresColumnas.get(etiqueta)+this.filaActual);
			return celda.getContents().trim();
		}
	}
	
	/**
	 * Establece el puntero en la fila que se indique en el par�metro. (S�lo para KDT).
	 * @param filaActual
	 * @throws Exception 
	 */
	public void setFilaActual(int filaActual) throws Exception{
		if (!this.modo.equals("KDT")){
			throw new Exception("No se puede ejecutar esta acci�n sin activar el modo \"KDT\". Act�velo en el constructor para usar esta funcionalidad");
		}else{
			if (filaActual<1){
				throw new Exception("Indice incorrecto. El �ncide debe ser mayor o igual a 1, para comenzar en la primera fila de la excel");
			}else{
				this.filaActual = filaActual+1;
			}
		}
	}

	
	
	//TODO
	public String getCelda(int fila, String columna){
	return "";
	}
	
	public String getExternalID(String nombreCaso, String externalid){
		int totalNoOfRows = hojaExcel.getRows();
		int totalNoOfCols = hojaExcel.getColumns();
		String dato="";
		boolean finFilas = false;
		boolean finColumnas = false;
		int posicionFila = 0;
		int posicionColumna = 0;
		
		for (int col=0; ((col<totalNoOfCols)&&(!finColumnas)); col++) {
        	if(hojaExcel.getCell(col, 0).getContents().equals(externalid) ){
        		posicionColumna = col;
        		finColumnas = true;
        		for (int row=0; ((row<totalNoOfRows)&&(!finFilas)); row++) {
        			if(hojaExcel.getCell(0, row).getContents().equals(nombreCaso) ){
                		posicionFila = row;
                		finFilas = true;
                	}
        		}        		
        	}
        }
		dato = hojaExcel.getCell(posicionColumna, posicionFila).getContents();
		return dato;
	}	
	
	public String getCelda(int fila, int columna){
		String contenidoCelda = "";
		Cell celda = hojaExcel.getCell(columna, fila);
		String[] cotenidoCeldaDividido = celda.getContents().split(":");
		if (cotenidoCeldaDividido.length>0){
			contenidoCelda = cotenidoCeldaDividido[1];
		}else{
			contenidoCelda = cotenidoCeldaDividido[0];
		}
		return contenidoCelda;
	}
}
