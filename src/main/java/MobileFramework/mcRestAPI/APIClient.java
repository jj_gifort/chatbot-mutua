package MobileFramework.mcRestAPI;

import okhttp3.*;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import MobileFramework.Properties.GlobalProperties;

public class APIClient {

	// *************************************
	// Mobile Center instance data
	// *************************************

	private static String BASE_URL = "http://" + GlobalProperties.appiumHostMC + ":" + GlobalProperties.appiumPortMC
			+ "/rest/";
	//private static String username = GlobalProperties.MC_USER;
	//private static String password = GlobalProperties.MC_PASS;

	// ************************************
	// Mobile Center APIs end-points
	// ************************************

	private static final String ENDPOINT_CLIENT_LOGIN = "client/login";
	private static final String ENDPOINT_CLIENT_LOGOUT = "client/logout";
	private static final String ENDPOINT_CLIENT_DEVICES = "deviceContent";
	private static final String ENDPOINT_CLIENT_APPS = "apps";
	private static final String ENDPOINT_CLIENT_INSTALL_APPS = "apps/install";
	private static final String ENDPOINT_CLIENT_UPLOAD_APPS = "apps/upload?enforceUpload=true";
	private static final String ENDPOINT_CLIENT_USERS = "v2/users";
	private static final String ENDPOINT_CLIENT_RESERVATIONS = "v2/reservation";
	private static final String ENDPOINT_CLIENT_LICENSE = "v2/license";

	// ************************************
	// Initiate proxy configuration
	// ************************************

	private static final boolean USE_PROXY = false;
	private static final String PROXY = "http://10.0.8.102:8080";

	// ************************************
	// Path to app (IPA or APK) for upload
	// ************************************

	@SuppressWarnings("unused")
	//TODO
	private static String APP = ""; //GlobalProperties.AndroidAppPath || GlobalProperties.iOSAppPath;

	private OkHttpClient client;
	private String hp4msecret;
	private String jsessionid;
	private String responseBodyStr;

	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private static final MediaType APK = MediaType.parse("application/vnd.android.package-archive");

	public APIClient(String username, String password) {
		OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder().readTimeout(240, TimeUnit.SECONDS)
				.writeTimeout(240, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).cookieJar(new CookieJar() {
					private final HashMap<String, List<Cookie>> cookieStore = new HashMap<>();

					@Override
					public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
						List<Cookie> storedCookies = cookieStore.get(url.host());
						if (storedCookies == null) {
							storedCookies = new ArrayList<>();
							cookieStore.put(url.host(), storedCookies);
						}
						storedCookies.addAll(cookies);
						for (Cookie cookie : cookies) {
							if (cookie.name().equals("hp4msecret"))
								hp4msecret = cookie.value();
							if (cookie.name().equals("JSESSIONID"))
								jsessionid = cookie.value();
						}
					}

					@Override
					public List<Cookie> loadForRequest(HttpUrl url) {
						List<Cookie> cookies = cookieStore.get(url.host());
						return cookies != null ? cookies : new ArrayList<>();
					}
				});

		if (USE_PROXY) {
			int PROXY_PORT = 8080;
			clientBuilder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(PROXY, PROXY_PORT)));
		}

		client = clientBuilder.build();
		try {
			login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void login(String username, String password) throws IOException {

		String strCredentials = "{\"name\":\"" + username + "\",\"password\":\"" + password + "\"}";
		RequestBody body = RequestBody.create(JSON, strCredentials);
		executeRestAPI(ENDPOINT_CLIENT_LOGIN, HttpMethods.POST, body);

	}

	public String apps() throws IOException {
		return executeRestAPI(ENDPOINT_CLIENT_APPS);
	}

	public String reservations() throws IOException {
		return executeRestAPI(ENDPOINT_CLIENT_RESERVATIONS);
	}

	public String users() throws IOException {
		return executeRestAPI(ENDPOINT_CLIENT_USERS);

	}

	public String deviceContent() throws IOException {
		return executeRestAPI(ENDPOINT_CLIENT_DEVICES);
	}

	public void installApp(String package_name, String version, String device_id, Boolean is_intrumented)
			throws IOException {
		String counter = version;
		String str = "{\n" + "  \"app\": {\n" + "    \"counter\": " + counter + ",\n" + "    \"id\": \"" + package_name
				+ "\",\n" + "    \"instrumented\": " + (is_intrumented ? "true" : "false") + "\n" + "  },\n"
				+ "  \"deviceCapabilities\": {\n" + "    \"udid\": \"" + device_id + "\"\n" + "  }\n" + "}";
		RequestBody body = RequestBody.create(JSON, str);
		executeRestAPI(ENDPOINT_CLIENT_INSTALL_APPS, HttpMethods.POST, body);
	}

	/**
	 * Install application by file name, when there are multiple matches for file
	 * name in database, will select first application.
	 **/

	public void installAppByFileAndDeviceID(String filename, String device_id, Boolean is_intrumented)
			throws IOException {
		apps();
		String[] res = responseBodyStr.split(filename);
		if (res == null || res.length < 2) {
			return;
		} else {
			String counter = parseProperty(res[0], "\"counter\":", ",");
			String package_name = parseProperty(res[1], "\"identifier\":\"", "\",");
			installApp(package_name, counter, device_id, is_intrumented);
		}
	}

	public void logout() throws IOException {
		RequestBody body = RequestBody.create(JSON, "");
		executeRestAPI(ENDPOINT_CLIENT_LOGOUT, HttpMethods.POST, body);
	}

	public String executeRestAPI(String endpoint) throws IOException {
		return executeRestAPI(endpoint, HttpMethods.GET);
	}

	public String executeRestAPI(String endpoint, HttpMethods httpMethod) throws IOException {
		return executeRestAPI(endpoint, httpMethod, null);
	}

	public String executeRestAPI(String endpoint, HttpMethods httpMethod, RequestBody body) throws IOException {

		final ResponseBody responseBody;

		// build the request URL and headers
		Request.Builder builder = new Request.Builder().url(BASE_URL + endpoint)
				.addHeader("Content-type", JSON.toString()).addHeader("Accept", JSON.toString());

		// add CRSF header
		if (hp4msecret != null) {
			builder.addHeader("x-hp4msecret", hp4msecret);
		}

		switch (httpMethod) {
		case POST:
			builder.post(body);
			break;
		case PUT:
			builder.put(body);
			break;
		case DELETE:
			builder.delete(body);
			break;
		default:
			builder.get();
		}

		Request request = builder.build();
		System.out.println("\n" + request);

		try (Response response = client.newCall(request).execute()) {
			if (response.isSuccessful()) {
//				System.out.println(response.toString());
				responseBody = response.body();
				if (responseBody != null) {
					responseBodyStr = responseBody.string();
//					System.out.println("Body: " + responseBodyStr);
					return responseBodyStr;

				}
			} else {
				throw new IOException("Unexpected code " + response);
			}
			response.close();
		}

		return responseBodyStr;

	}

	@SuppressWarnings("unused")
	public void uploadApp(String filename) throws IOException {
		String[] parts = filename.split("\\\\");
		System.out.println("Uploading and preparing the app... ");
		RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
				.addFormDataPart("file", parts[parts.length - 1], RequestBody.create(APK, new File(filename))).build();

		Request request = new Request.Builder().addHeader("content-type", "multipart/form-data")
				.addHeader("x-hp4msecret", hp4msecret).addHeader("JSESSIONID", jsessionid)
				.url(BASE_URL + ENDPOINT_CLIENT_UPLOAD_APPS).post(requestBody).build();

		try (Response response = client.newCall(request).execute()) {
			if (response.isSuccessful()) {
				System.out.println("Done!");
				System.out.println(response.toString());
				ResponseBody body = response.body();
				if (body != null) {
					System.out.println(body.string());
				}
			} else {
				throw new IOException("Unexpected code " + response);
			}
			response.close();

		}

	}

	private String parseProperty(String source, String prefix, String suffix) {
		try {
			String[] array = source.split(prefix);
			String str = array[array.length - 1];
			return str.split(suffix)[0];
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void deleteReservation(String deviceName) throws IOException {

		String responseBody = reservations();
		String reservationUuid = lookForReservationUid(responseBody, deviceName);
		String tuple = "{\"uid\":\"" + reservationUuid + "\"}";
		executeRestAPI(ENDPOINT_CLIENT_RESERVATIONS + "/" + reservationUuid, HttpMethods.DELETE);

	}

	private String lookForReservationUid(String responseBody, String deviceName) throws IOException {

		String uuid = "";

		try {
			JSONArray jsonArray = new JSONArray(responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				if (jsonArray.get(i) instanceof JSONObject) {
					JSONObject jsnObj = (JSONObject) jsonArray.get(i);
					String finalValue = (String) jsnObj.getJSONObject("device").get("logicName");
					if (finalValue.equals(deviceName)) {
						uuid = (String) jsnObj.get("uuid");
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return uuid;
	}

	/*public Boolean checkAvailability(String deviceName) throws IOException {

		Boolean availability = false;
		String responseBody = deviceContent();

		try {
			JSONArray jsonArray = new JSONArray(responseBody);
			for (int i = 0; i < jsonArray.length(); i++) {
				if (jsonArray.get(i) instanceof JSONObject) {
					JSONObject jsnObj = (JSONObject) jsonArray.get(i);
					String finalValue = (String) jsnObj.get("nickName");
					if (finalValue.equals(deviceName)) {
						Boolean connected = (Boolean) jsnObj.get("connected");
						String free = (String) jsnObj.getJSONObject("currentReservation").get("status");
						if (connected == true && free.equals("Free"))
							availability = true;
						break; // Mejorar esto
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return availability;
	}*/

}