package MobileFramework.Base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.HasSupportedPerformanceDataType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import logger.Log;
import okhttp3.MediaType;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.IMethodInstance;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.service.ExtentTestManager;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;

import MobileFramework.Performance.AndroidPerformance;
import MobileFramework.Properties.GlobalProperties;
import MobileFramework.Properties.PropertiesManager;
import MobileFramework.mcRestAPI.APIClient;

/**
 * contains all the methods to create a new session and destroy the session
 * after the test(s) execution is over. Each test extends this class.
 */

@Listeners({ ExtentITestListenerClassAdapter.class })
public class CreateSession {

	protected AppiumDriver driver = null;
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private static String OS;
	private static String serverHost;
	private static String serverPort;
	private static String DEVICE_NAME;
	private static APIClient mcApiClient;

	/**
	 * this method starts Appium server. Calls startAppiumServer method to start the
	 * session depending upon your OS.
	 * 
	 * @throws Exception Unable to start appium server
	 */

	// @BeforeSuite
	public void invokeAppium() throws Exception {
		String OS = System.getProperty("android").toLowerCase();
		try {
			startAppiumServer(OS);
			Log.info("Appium server started successfully");
		} catch (Exception e) {
			Log.logError(getClass().getName(), "startAppium", "Unable to start appium server");
			throw new Exception(e.getMessage());
		}
	}

	public AppiumDriver getDriver() {
		return driver;
	}

	public void setDriver(AppiumDriver driver) {
		this.driver = driver;
	}

	/**
	 * this method stops Appium server.Calls stopAppiumServer method to stop session
	 * depending upon your OS.
	 * 
	 * @throws Exception Unable to stop appium server
	 */
	// @AfterSuite
	public void stopAppium() throws Exception {
		try {
			stopAppiumServer(OS);
			Log.info("Appium server stopped successfully");

		} catch (Exception e) {
			Log.logError(getClass().getName(), "stopAppium", "Unable to stop appium server");
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * this method creates the driver depending upon the passed parameter (android
	 * or iOS) and loads the properties files (config and test data properties
	 * files).
	 * 
	 * @param os         android or iOS
	 * @param methodName - name of the method under execution
	 * @throws Exception issue while loading properties files or creation of driver.
	 */

	@Parameters({ "os" })
	@BeforeMethod
	public void createDriver(String os, Method methodName) throws Exception {

		GlobalProperties.readGlobalProperties();
		PropertiesManager propMng = new PropertiesManager();
		propMng.propertiesFileLoad(os);
		propMng.configureLogger();

	//	if (mcApiClient == null)
	//		mcApiClient = new APIClient(GlobalProperties.MC_USER, GlobalProperties.MC_PASS);

		serverHost = GlobalProperties.appiumHostMC;
		serverPort = GlobalProperties.appiumPortMC;
		DEVICE_NAME = GlobalProperties.DEVICE_NAME;

		String connectionString = "http://" + serverHost + ":" + serverPort + "/wd/hub";

		if (os.equalsIgnoreCase("android")) {
			String buildPath = choosebuild(os);
			androidDriver(buildPath, methodName, connectionString);

		} else if (os.equalsIgnoreCase("iOS")) {
			String buildPath = choosebuild(os);
			iOSDriver(buildPath, methodName, connectionString);
		}

	}

	@AfterMethod
	public synchronized void afterMethod(ITestResult result) {

		// Quit driver and reservations on MC
//		tearDown();

		switch (result.getStatus()) {
		case ITestResult.FAILURE:
			try {
				ExtentTestManager.getTest(result).fail("ITestResult.FAILURE, event afterMethod")
						.addScreenCaptureFromPath(
								GlobalProperties.screenShotPath + "android/LoginVerification-verifyLoginScenario.jpg");
			} catch (IOException e) {
				e.printStackTrace();
			}
		case ITestResult.SKIP:
			ExtentTestManager.getTest(result).skip("ITestResult.SKIP, event afterMethod");
			break;
		default:
			ExtentTestManager.getTest(result).pass("default, event afterMethod");
			break;
		}
	}

	/**
	 * This method quit the driver after the execution of test(s)7 Instanciate a
	 * mobile center client to erase device reservation
	 */

	@AfterMethod
	public void tearDown() {

		if (driver != null) {
			Log.info("Shutting down driver");
			driver.quit();
		}

	/*	if (!checkFordeviceAvailability(DEVICE_NAME)) {
			Log.info("Deleting MC reservation");
			try {
				mcApiClient.deleteReservation(DEVICE_NAME);
			} catch (IOException e) {
				e.printStackTrace();
			}
			Log.info("Reservation deleted");
		}*/

	}

	/**
	 * this method creates the android driver
	 * 
	 * @param buildPath  - path to pick the location of the app
	 * @param methodName - name of the method under execution
	 * @throws Exception
	 */
	public synchronized void androidDriver(String buildPath, Method methodName, String connectionString)
			throws Exception {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		//capabilities.setCapability("userName", GlobalProperties.MC_USER);
		//capabilities.setCapability("password", GlobalProperties.MC_PASS);
		capabilities.setCapability("deviceName", GlobalProperties.DEVICE_NAME);
		capabilities.setCapability("udid", GlobalProperties.deviceAndroidUDID);
		capabilities.setCapability("appPackage", GlobalProperties.androidPackageName);
		capabilities.setCapability("appActivity", GlobalProperties.androidActivityName);
		capabilities.setCapability("name", methodName.getName());
		capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
		capabilities.setCapability("automationName", "UiAutomator2");

	//	if (checkFordeviceAvailability(DEVICE_NAME)) {
			Log.info("Launching test");
			AndroidDriver driver = new AndroidDriver(new URL(connectionString), capabilities);
			setDriver(driver);
			
	/*		ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
			executorService.scheduleWithFixedDelay(
			    new AndroidPerformance(driver), 0, 30, TimeUnit.SECONDS);
			*/
//			showPerformanceMetrics(driver);	
			
	/*	} else {
			Log.info("Skipping test");
		}*/
	}

	private void showPerformanceMetrics(AndroidDriver driver) throws InterruptedException {

		while (true) {
		    try {
		        Future<?> future = Executors.newSingleThreadExecutor().submit(
		                    new AndroidPerformance(driver));
		        future.get(); 
		    } catch (Exception e) {
		    }
		    Thread.sleep(10000);
		}
	}

	/**
	 * this method creates the iOS driver
	 * 
	 * @param buildPath- path to pick the location of the app
	 * @param methodName- name of the method under execution
	 * @throws IOException
	 */
	public void iOSDriver(String buildPath, Method methodName, String connectionString) throws IOException {

		File app = new File(buildPath);

		String deviceiOSName = GlobalProperties.deviceiOSName;
		String deviceiOSUDID = GlobalProperties.deviceiOSUDID;
		String deviceiOSVersion = GlobalProperties.deviceiOSVersion;
		String applicationBundle = GlobalProperties.applicationBundle;
		String iOSAppPath = GlobalProperties.iOSAppPath;

		DesiredCapabilities capabilities = new DesiredCapabilities();
		//capabilities.setCapability("userName", GlobalProperties.MC_USER);
		//capabilities.setCapability("password", GlobalProperties.MC_PASS);
		capabilities.setCapability("deviceName", deviceiOSName);
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("platformVersion", deviceiOSVersion);
		capabilities.setCapability("name", methodName.getName());
		capabilities.setCapability(MobileCapabilityType.UDID, deviceiOSUDID);
		capabilities.setCapability("bundleId", applicationBundle);

		//if (checkFordeviceAvailability(DEVICE_NAME)) {
			driver = new IOSDriver(new URL(connectionString), capabilities);
			setDriver(driver);

			Log.info("iOS driver created");

		} 
		//	else
		//	Log.info("Device is not available");

//	}

/*	private Boolean checkFordeviceAvailability(String deviceName) {

		Boolean availability = false;
		try {
			Log.info("Checking device reservation");
			availability = mcApiClient.checkAvailability(deviceName);
			Log.info(availability ? "Device available" : "Device is not available");

		} catch (IOException e) {
			e.printStackTrace();
		}
		return availability;
	}*/

	/**
	 * this method starts the appium server depending on your OS.
	 * 
	 * @param os your machine OS (windows/linux/mac)
	 * @throws IOException          Signals that an I/O exception of some sort has
	 *                              occurred
	 * @throws ExecuteException     An exception indicating that the executing a
	 *                              subprocesses failed
	 * @throws InterruptedException Thrown when a thread is waiting, sleeping, or
	 *                              otherwise occupied, and the thread is
	 *                              interrupted, either before or during the
	 *                              activity.
	 */

	public void startAppiumServer(String os) throws ExecuteException, IOException, InterruptedException {

		switch (os) {

		case "windows":
			startWindowsAppiumServer();
			break;
		case "mac os x":
			startMacOsAppiumServer();
			break;
		case "linux":
			startLinuxAppiumServer();
			break;
		default:
			Log.info("OS not supported yet");
		}
	}

	private void startLinuxAppiumServer() throws ExecuteException, IOException {

		System.getenv("ANDROID_HOME");
		// System.out.println("PATH :" +System.getenv("PATH"));
		CommandLine command = new CommandLine("/bin/bash");
		command.addArgument("-c");
		command.addArgument("~/.linuxbrew/bin/node");
		command.addArgument("~/.linuxbrew/lib/node_modules/appium/lib/appium.js", true);
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);
	}

	private void startMacOsAppiumServer() throws ExecuteException, IOException {

		CommandLine command = new CommandLine("/Applications/Appium.app/Contents/Resources/node/bin/node");
		command.addArgument("/Applications/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js", false);
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4723");
		command.addArgument("--full-reset", false);
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);
	}

	private void startWindowsAppiumServer() throws ExecuteException, IOException {

		CommandLine command = new CommandLine("cmd");
		command.addArgument("/c");
		command.addArgument("C:/Program Files/nodejs/node.exe");
		command.addArgument("C:/Appium/node_modules/appium/bin/appium.js");
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4723");
		command.addArgument("--full-reset", false);

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);

	}

	/**
	 * this method stops the appium server.
	 * 
	 * @param os your machine OS (windows/linux/mac).
	 * @throws IOException      Signals that an I/O exception of some sort has
	 *                          occurred.
	 * @throws ExecuteException An exception indicating that the executing a
	 *                          subprocesses failed.
	 */
	public void stopAppiumServer(String os) throws ExecuteException, IOException {
		if (os.contains("windows")) {
			CommandLine command = new CommandLine("cmd");
			command.addArgument("/c");
			command.addArgument("Taskkill /F /IM node.exe");

			DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
			DefaultExecutor executor = new DefaultExecutor();
			executor.setExitValue(1);
			executor.execute(command, resultHandler);
		} else if (os.contains("mac os x")) {
			String[] command = { "/usr/bin/killall", "-KILL", "node" };
			Runtime.getRuntime().exec(command);
			Log.info("Appium server stopped");
		} else if (os.contains("linux")) {
			// need to add it
		}
	}

	public String choosebuild(String invokeDriver) {
		String appPath = null;
		if (invokeDriver.equalsIgnoreCase("android")) {
			appPath = GlobalProperties.AndroidAppPath;
			return appPath;
		} else if (invokeDriver.equalsIgnoreCase("iOS")) {
			appPath = GlobalProperties.iOSAppPath;
			return appPath;
		}

		return appPath;
	}

}
